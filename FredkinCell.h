
#ifndef FredkinCell_h
#define  FredkinCell_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <string>   // string
#include "AbstractCell.h"
using namespace std;

class Fredkin : public AbstractCell
{
    template<typename T> friend class Life;
	friend class Cell;
private:
    e_CELL type;
    int age;

    /**
    *  return and update the next state of the cell
    *  @param n_state number of state neighbors
    **/
    bool get_next_state(int n_state)
    {
        if(state)
            next_state = n_state == 1 || n_state == 3;
        else
            next_state = n_state == 1 || n_state == 3;
        if(state && next_state)
            ++age;
        return next_state;
    }

    /**
    * return cell's string representation
    **/
    string to_string() const
    {
        if(state)
        {
            if(age < 10)
                return std::to_string(age);
            else
                return "+";
        }
        return "-";
    }

    /**
    *  return type of the cell
    **/
    e_CELL get_type() const
    {
        return type;
    }
public:
    Fredkin() : AbstractCell(), type(e_CELL::FredkinCell), age(0) {}

    Fredkin(const Fredkin& rhs) = default;

};

#endif //  FredkinCell_h