
#ifndef ConwayCell_h
#define  ConwayCell_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <string>   // string
#include "AbstractCell.h"
using namespace std;

class Conway : public AbstractCell
{
    template<typename T> friend class Life;

private:
    e_CELL type;

    /**
    *  return and update the next state of the cell
    *  @param n_state number of state neighbors
    **/
    bool get_next_state(int n_state)
    {
        if(state)
            next_state = n_state == 2 || n_state == 3;
        else
            next_state = n_state == 3;
        return next_state;

    }

    /**
    * return cell's string representation
    **/
    string to_string() const
    {
        return state ? "*" : ".";
    }


    /**
    *  return type of the cell
    **/
    e_CELL get_type() const
    {
        return type;
    }
public:
    /**
     * Default Constructor
     */
    Conway() : AbstractCell(), type(e_CELL::ConwayCell) {}

    Conway(const Conway& rhs) = default;

};

#endif //  ConwayCell_h