// ---------------------------------------
// projects/c++/life/RunLifeFredkin.c++
// Copyright (C) 2018
// David Vu
// ---------------------------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <fstream>  // ifstream
#include <iostream> // cout, endl, getline
#include <sstream>  // istringstream
#include <string>   // ss
#include "FredkinCell.h"
#include "Life.h"

using namespace std;




// ----
// main
// ----

int main () {
    istream& r = cin;
    ostream& w = cout;

    string s;
    getline(r,s);

    int num_tests = stoi(s);
    int first_test = num_tests;
    while(num_tests-- > 0) {
        if(first_test != num_tests + 1)
            w << endl;
        getline(r,s);//blank line

        getline(r,s); // dimension line
        string l1(s);
        istringstream f(l1);
        getline(f,s,' ');
        int num_r = stoi(s);
        getline(f,s,' ');
        int num_c = stoi(s);
        getline(r,s); //num cells
        int n_cells = stoi(s);
        Life<Fredkin> game(num_r, num_c);
        while(n_cells--)
        {
            getline(r,s);
            string l2(s);
            istringstream f2(l2);
            getline(f2,s,' ');
            int r = stoi(s);
            getline(f2,s,' ');
            int c = stoi(s);
            game.add_cell(r,c);
        }
        getline(r,s);
        string l3(s);
        istringstream f3(l3);
        getline(f3,s,' ');
        int ticks = stoi(s);
        getline(f3,s,' ');
        int freq = stoi(s);
        int t = 0;
        w << "*** Life<FredkinCell> " << num_r << "x" << num_c << " ***"<< endl;
        w << endl;
        game.write_board(w);
        while(ticks-- > 0)
        {
            game.tick();
            ++t;
            if(!(t % freq))
            {
                w << endl;
                game.write_board(w);
            }
        }
    }
    return 0;
}
