// -----------------------------------------
// projects/c++/life/TestLife.c++
// Copyright (C) 2018
// David Vu
// -----------------------------------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/Primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/AdvancedGuide.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Life.h"
#include "Cell.h"
#include "FredkinCell.h"


// Cell tests
TEST(myTestLife, testCell) {
    ostringstream w;
    Life<Cell> d(4, 4);
    d.add_cell(1,1);
    d.write_board(w);
    string s = "Generation = 0, Population = 1.\n----\n-0--\n----\n----\n";
    ASSERT_EQ(w.str(), s);
}


TEST(myTestLife, testCell2) { //adding to same spot
    ostringstream w;
    Life<Cell> d(4, 4);
    d.add_cell(1,1);
    d.add_cell(1,1);
    d.write_board(w);
    string s = "Generation = 0, Population = 1.\n----\n-0--\n----\n----\n";
    ASSERT_EQ(w.str(), s);
}

TEST(myTestLife, testCell3) {
    ostringstream w;
    Life<Cell> d(4, 4);
    d.add_cell(1,1);
    d.add_cell(1,2);
    d.add_cell(1,3);
    d.add_cell(0,1);
    d.write_board(w);
    string s = "Generation = 0, Population = 4.\n-0--\n-000\n----\n----\n";
    ASSERT_EQ(w.str(), s);
}


TEST(myTestLife, testCell4) { //tick tests
    ostringstream w;
    Life<Cell> d(4, 4);
    d.add_cell(1,1);
    d.add_cell(1,2);
    d.add_cell(1,3);
    d.add_cell(0,1);
    d.tick();
    d.tick();
    d.tick();
    d.write_board(w);
    string s = "Generation = 3, Population = 8.\n-*--\n*101\n---0\n0--1\n";
    ASSERT_EQ(w.str(), s);
}


TEST(myTestLife, testCell5) { //tick tests
    ostringstream w;
    Life<Cell> d(10, 10);
    d.add_cell(3,1);
    d.add_cell(3,2);
    d.add_cell(4,2);
    d.add_cell(9,9);
    d.tick();
    d.tick();
    d.tick();
    d.tick();
    d.tick();
    d.tick();
    d.tick();
    d.tick();
    d.tick();
    d.tick();
    d.write_board(w);
    string s = "Generation = 10, Population = 29.\n.0*1------\n0**----0-0\n.1.---.---\n...**.-.-1\n.-1-.-----\n-*1---.---\n01*-0-----\n--1.------\n1-*-1-----\n0*1*10----\n";
    ASSERT_EQ(w.str(), s);
}


// Fredkin tests
TEST(myTestLife, testFredkin) {
    ostringstream w;
    Life<Fredkin> d(4, 4);
    d.add_cell(1,1);
    d.write_board(w);
    string s = "Generation = 0, Population = 1.\n----\n-0--\n----\n----\n";
    ASSERT_EQ(w.str(), s);
}


TEST(myTestLife, testFredkin2) { //adding to same spot
    ostringstream w;
    Life<Fredkin> d(4, 4);
    d.add_cell(1,1);
    d.add_cell(1,1);
    d.write_board(w);
    string s = "Generation = 0, Population = 1.\n----\n-0--\n----\n----\n";
    ASSERT_EQ(w.str(), s);
}

TEST(myTestLife, testFredkin3) {
    ostringstream w;
    Life<Fredkin> d(4, 4);
    d.add_cell(1,1);
    d.add_cell(1,2);
    d.add_cell(1,3);
    d.add_cell(0,1);
    d.write_board(w);
    string s = "Generation = 0, Population = 4.\n-0--\n-000\n----\n----\n";
    ASSERT_EQ(w.str(), s);
}


TEST(myTestLife, testFredkin4) { //fredkin tests
    ostringstream w;
    Life<Fredkin> d(4, 4);
    d.add_cell(1,1);
    d.add_cell(1,2);
    d.add_cell(1,3);
    d.add_cell(0,1);
    d.tick();
    d.tick();
    d.tick();
    d.tick();
    d.tick();
    d.tick();
    d.write_board(w);
    string s = "Generation = 6, Population = 8.\n-6-1\n54--\n-1--\n-004\n";
    ASSERT_EQ(w.str(), s);
}


TEST(myTestLife, testFredkin5) { //tick tests
    ostringstream w;
    Life<Fredkin> d(10, 10);
    d.add_cell(3,1);
    d.add_cell(3,2);
    d.add_cell(4,2);
    d.add_cell(9,9);
    d.tick();
    d.tick();
    d.tick();
    d.write_board(w);
    string s = "Generation = 3, Population = 28.\n-00-------\n01-0------\n0-2-0-----\n21-2-0----\n1-1-10----\n-1-00-----\n--10-----0\n--0-----0-\n-------0-0\n------0-0-\n";
    ASSERT_EQ(w.str(), s);
}


// Conway tests
TEST(myTestLife, testConway) {
    ostringstream w;
    Life<Conway> d(4, 4);
    d.add_cell(1,1);
    d.write_board(w);
    string s = "Generation = 0, Population = 1.\n....\n.*..\n....\n....\n";
    ASSERT_EQ(w.str(), s);
}


TEST(myTestLife, testConway2) { //adding to same spot
    ostringstream w;
    Life<Conway> d(4, 4);
    d.add_cell(1,1);
    d.add_cell(1,1);
    d.write_board(w);
    string s = "Generation = 0, Population = 1.\n....\n.*..\n....\n....\n";
    ASSERT_EQ(w.str(), s);
}

TEST(myTestLife, testConway3) {
    ostringstream w;
    Life<Conway> d(4, 4);
    d.add_cell(1,1);
    d.add_cell(1,2);
    d.add_cell(1,3);
    d.add_cell(0,1);
    d.write_board(w);
    string s = "Generation = 0, Population = 4.\n.*..\n.***\n....\n....\n";
    ASSERT_EQ(w.str(), s);
}


TEST(myTestLife, testConway4) { //fredkin tests
    ostringstream w;
    Life<Conway> d(4, 4);
    d.add_cell(1,1);
    d.add_cell(1,2);
    d.add_cell(1,3);
    d.add_cell(0,1);
    d.tick();
    d.tick();
    d.tick();
    d.tick();
    d.tick();
    d.tick();
    d.write_board(w);
    string s = "Generation = 6, Population = 6.\n.**.\n*..*\n.**.\n....\n";
    ASSERT_EQ(w.str(), s);
}


TEST(myTestLife, testConway5) { //tick tests
    ostringstream w;
    Life<Conway> d(10, 10);
    d.add_cell(3,1);
    d.add_cell(3,2);
    d.add_cell(4,2);
    d.add_cell(9,9);
    d.tick();
    d.tick();
    d.tick();
    d.write_board(w);
    string s = "Generation = 3, Population = 4.\n..........\n..........\n..........\n.**.......\n.**.......\n..........\n..........\n..........\n..........\n..........\n";
    ASSERT_EQ(w.str(), s);
}



