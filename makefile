.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules

FILES :=                                  \
    .gitignore                            \
    Life-tests      	                  \
    Life.h                           \
    makefile                             \
    RunLifeConway.c++                      \
    RunLifeFredkin.c++                      \
    RunLifeCell.c++                      \
    RunLifeConway.in                       \
    RunLifeConway.out                      \
    RunLifeCell.in                       \
    RunLifeCell.out                      \
    RunLifeFredkin.in                       \
    RunLifeFredkin.out                      \
    TestLife.c++                    \

# uncomment these four lines when you've created those files
# you must replace GitLabID with your GitLabID
#    life-tests/GitLabID-RunLife.in  \
#    life-tests/GitLabID-RunLife.out \
#    Life.log                             \
#    html                                      \

hr_conway:
	(cat AbstractCell.h  && cat ConwayCell.h && cat FredkinCell.h  && cat Cell.h  && cat Life.h && cat RunLifeConway.c++)  \
	| sed '/^#include "AbstractCell.h"/d;' | sed '/^#include "ConwayCell.h"/d;' \
	| sed '/^#include "Life.h"/d;' | sed '/^#include "FredkinCell.h"/d;' | sed '/^#include "Cell.h"/d;' > hacker.c++

	cat hacker.c++ 

hr_fredkin:
	(cat AbstractCell.h  && cat ConwayCell.h && cat FredkinCell.h  && cat Cell.h  && cat Life.h && cat RunLifeFredkin.c++)  \
	| sed '/^#include "AbstractCell.h"/d;' | sed '/^#include "ConwayCell.h"/d;' \
	| sed '/^#include "Life.h"/d;' | sed '/^#include "FredkinCell.h"/d;' | sed '/^#include "Cell.h"/d;' > hacker.c++

	cat hacker.c++ 

hr_cell:
	(cat AbstractCell.h  && cat ConwayCell.h && cat FredkinCell.h  && cat Cell.h  && cat Life.h && cat RunLifeCell.c++)  \
	| sed '/^#include "AbstractCell.h"/d;' | sed '/^#include "ConwayCell.h"/d;' \
	| sed '/^#include "Life.h"/d;' | sed '/^#include "FredkinCell.h"/d;' | sed '/^#include "Cell.h"/d;' > hacker.c++

	cat hacker.c++ 

Life-tests:
	git clone https://gitlab.com/gpdowning/cs371p-Life-tests.git Life-tests

html: Doxyfile Life.h
	doxygen Doxyfile

Life.log:
	git log > Life.log

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATEIC to YES
Doxyfile:
	doxygen -g

RunLifeConway: Life.h RunLifeConway.c++
	-cppcheck RunLifeConway.c++
	g++ -pedantic -std=c++14 -Wall -Weffc++ -Wextra RunLifeConway.c++ -o RunLifeConway

RunLifeFredkin: Life.h RunLifeFredkin.c++
	-cppcheck RunLifeFredkin.c++
	g++ -pedantic -std=c++14 -Wall -Weffc++ -Wextra RunLifeFredkin.c++ -o RunLifeFredkin

RunLifeCell: Life.h RunLifeCell.c++
	-cppcheck RunLifeCell.c++
	g++ -O0 -pedantic -std=c++14 -Wall -Weffc++ -Wextra RunLifeCell.c++ -o RunLifeCell

RunLifeConway.c++x: RunLifeConway
	./RunLifeConway < RunLifeConway.in > RunLifeConway.tmp
	-diff RunLifeConway.tmp RunLifeConway.out

RunLifeFredkin.c++x: RunLifeFredkin
	./RunLifeFredkin < RunLifeFredkin.in > RunLifeFredkin.tmp
	-diff RunLifeFredkin.tmp RunLifeFredkin.out

RunLifeCell.c++x: RunLifeCell
	./RunLifeCell < RunLifeCell.in > RunLifeCell.tmp
	-diff RunLifeCell.tmp RunLifeCell.out



TestLife: Life.h TestLife.c++
	-cppcheck TestLife.c++
	g++ -fprofile-arcs -ftest-coverage -pedantic -std=c++14 -Wall -Weffc++ -Wextra TestLife.c++ -o TestLife -lgtest -lgtest_main -pthread

TestLife.c++x: TestLife
	valgrind ./TestLife
	gcov -b TestLife.c++ | grep -A 5 "File '.*Life.h'"
	gcov -b TestLife.c++ | grep -A 5 "File '.*TestLife.c++'"

all: RunLifeFredkin RunLifeCell RunLifeConway TestLife

check: $(FILES)

clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunLifeConway
	rm -f RunLifeFredkin
	rm -f RunLifeCell
	rm -f TestLife
	rm -f hacker.c++

config:
	git config -l

docker:
	docker run -it -v $(PWD):/usr/Life -w /usr/Life gpdowning/gcc

format:
	astyle Life.h
	astyle ConwayCell.h
	astyle FredkinCell.h
	astyle Cell.h
	astyle RunLifeCell.c++
	astyle RunLifeFredkin.c++
	astyle RunLifeConway.c++
	astyle TestLife.c++

pull:
	make clean
	@echo
	git pull
	git status



run: RunLifeFredkin.c++x RunLifeConway.c++x RunLifeCell.c++x TestLife.c++x

scrub:
	make clean
	rm -f  Life.log
	rm -f  Doxyfile
	rm -rf Life-tests
	rm -rf html
	rm -rf latex

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

versions:
	which    astyle
	astyle   --version
	@echo
	dpkg -s  libboost-dev | grep 'Version'
	@echo
	which    cmake
	cmake    --version
	@echo
	which    cppcheck
	cppcheck --version
	@echo
	which    doxygen
	doxygen  --version
	@echo
	which    g++
	g++      --version
	@echo
	which    gcov
	gcov     --version
	@echo
	which    git
	git      --version
	@echo
	which    make
	make     --version
	@echo
	which    valgrind
	valgrind --version
	@echo
	which    vim
	vim      --version
