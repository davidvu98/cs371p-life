// ------------------------------
// Copyright (C) 2018
// David Vu
// ------------------------------

#ifndef Life_h
#define Life_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <string>   // string
#include <utility>  // pair
#include <vector>   // vector
#include <cstdlib>   // rand
#include <algorithm>    // std::swap
#include <cassert>
#include "AbstractCell.h"

#define CONWAY_NEIGHBORS 8
#define FREDKIN_NEIGHBORS 4

static int const conway_dir[8][2] = {{-1,-1},{-1,0},{-1,1},{0,-1},{0,1},{1,-1},{1,0},{1,1}};
static int const fredkin_dir[4][2] = {{-1,0},{0,-1},{0,1},{1,0}};



template<typename T>
class Life
{
private:
    vector<vector<T>> grid;
    int generation;
    int population;

    int count_live_neighbors(size_t r, size_t c)
    {
        assert(r >= 0 && r < grid.size() && c >= 0 && c < grid[r].size());
        e_CELL type = grid[r][c].get_type();
        int count = 0;
        switch(type)
        {
        case e_CELL::ConwayCell:
            for(size_t i = 0; i < CONWAY_NEIGHBORS; ++i)
                count += is_cell_alive(r + conway_dir[i][0], c + conway_dir[i][1]) ? 1 : 0;
            break;
        case e_CELL::FredkinCell:
            for(size_t i = 0; i < FREDKIN_NEIGHBORS; ++i)
                count += is_cell_alive(r + fredkin_dir[i][0], c + fredkin_dir[i][1]) ? 1 : 0;
            break;
        }
        return count;
    }

    bool is_cell_alive(size_t r, size_t c)
    {
        if(r >= 0 && r < grid.size() && c >= 0 && c < grid[r].size())
            return grid[r][c].is_alive();
        return false;
    }

public:
    /**
     * Constructor
     * @param r number of rows
     * @param c number of columns
     */
    Life(int r, int c) :
        grid(r, vector<T>(c))
        ,generation(0),
        population(0) {}


    /**
     * Add a new live cell to the grid
     * @param r rowth number
     * @param c colth number
     */
    void add_cell(size_t r, size_t c)
    {
        assert(r >= 0 && r < grid.size() && c >= 0 && c < grid[r].size());

        if(!grid[r][c].is_alive())
        {
            (grid[r][c]).set_alive();
            ++population;
        }

    }


    /**
     * Tick the generation
     */
    void tick()
    {

        /*update the next state of each cell on the grid, but at the same time
         reserving current state */
        for(size_t r = 0; r < grid.size(); ++r)
        {
            for(size_t c = 0; c < grid[r].size(); ++c)
            {
                bool current_state = grid[r][c].is_alive(); /* previous state before updating */
                /*comparing current state to next state*/
                if(current_state != grid[r][c].get_next_state(count_live_neighbors(r,c)))
                    population += current_state ? -1 : 1;
            }
        }

        /*update current state to next state */
        for(size_t r = 0; r < grid.size(); ++r)
        {
            for(size_t c = 0; c < grid[r].size(); ++c)
            {
                grid[r][c].update_state();
            }
        }
        ++generation;
    }

    /**
     * Write the board to an ostream
     * @param w ostream object
     */

    void write_board(ostream& w)
    {
        w << "Generation = " << generation << ", Population = " << population << "." << endl;

        for(size_t r = 0; r < grid.size(); ++r)
        {
            for(size_t c = 0; c < grid[r].size(); ++c)
            {
                w << grid[r][c].to_string();
            }
            w << endl;
        }
    }


};

#endif // Life_h
