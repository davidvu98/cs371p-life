#ifndef AbstractCell_h
#define AbstractCell_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <string>   // string
#include <utility>  // pair
#include <vector>   // vector
using namespace std;
enum e_CELL
{
    ConwayCell, FredkinCell
};
class AbstractCell
{
	friend class Cell;
	protected:
		bool state;
		bool next_state;
		virtual bool get_next_state(int n_state) = 0;
		virtual string to_string() const = 0;
		virtual e_CELL get_type() const = 0;

		/**
		* set the current state of the cell to alive
		**/
		void set_alive()
		{
			next_state = true;
			state = true;
		}

		/**
		* update state to next state
		**/
		void update_state()
		{
			state = next_state;
		}

		/**
		* return cell current alive state
		**/
		bool is_alive() const
		{
			return state;
		}
	public:
		/**
	     * Default Constructor
	     */
		AbstractCell() : state(false), next_state(false) {}


		virtual ~AbstractCell() = default;



};
#endif // AbstractCell_h
