var searchData=
[
  ['generation',['generation',['../classLife.html#a121e806be4dec87539325f0d5b5abacd',1,'Life']]],
  ['get_5fnext_5fstate',['get_next_state',['../classAbstractCell.html#af2cb547207ac7869c8237873893976cf',1,'AbstractCell::get_next_state()'],['../classCell.html#a1cb06d5b5b388c0fcfb00532ce2e4483',1,'Cell::get_next_state()'],['../classConway.html#ab8a8afcbfe3f350c047264fcb597de29',1,'Conway::get_next_state()'],['../classFredkin.html#ae2b60a5d67e0d7185e284176fa21ef57',1,'Fredkin::get_next_state()']]],
  ['get_5ftype',['get_type',['../classAbstractCell.html#aa3d13308fe81fbc64eb7562ad450442b',1,'AbstractCell::get_type()'],['../classCell.html#a84e81220c97e35ddc97469dc79559f03',1,'Cell::get_type()'],['../classConway.html#a65e63fd44267f79479ded286df6cc75b',1,'Conway::get_type()'],['../classFredkin.html#aebf99481e6f1b037bc98801d6e54e0be',1,'Fredkin::get_type()']]],
  ['grid',['grid',['../classLife.html#a2e3d70c08ebfce7a1f425d48d4d5acda',1,'Life']]]
];
