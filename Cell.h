
#ifndef Cell_h
#define  Cell_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <string>   // string
#include "ConwayCell.h"
#include "FredkinCell.h"
#include "AbstractCell.h"
using namespace std;


class Cell
{
    template<typename T> friend class Life;
private:
    AbstractCell *p;
    /**
    * @doc strings
    *
    **/
    bool get_next_state(int n_state)
    {
        p->get_next_state(n_state);
        if(p->get_type() == e_CELL::FredkinCell && (*(dynamic_cast<Fredkin*>(p))).age >= 2)
        {
            bool state = p->state;
            delete p;
            p = new Conway();
            p->state = state;
            p->next_state = true;
        }
        return p->next_state;
    }

    /**
    * return cell's string representations
    **/
    string to_string() const
    {
        return p->to_string();
    }

    /**
    * set the current state of the cell to alive
    **/
    void set_alive()
    {
        p->set_alive();
    }

    /**
    * update state to next state
    **/
    void update_state() const
    {
        p->update_state();
    }

    /**
    * get type of the cell
    **/
    e_CELL get_type() const
    {
        return p->get_type();
    }


    /**
    * return cell current alive state
    **/
    bool is_alive() const
    {
        return p->state;
    }

public:

    /**
     * Default Constructor
     */
    Cell() : p()
    {
        Fredkin *c = new Fredkin();
        p = c;
    }

    Cell& operator=(const Cell& rhs) = default;

    /**
     * Copy constructor
     * @param rhs right handside Cell
     */
    Cell(const Cell& rhs) : p() {
        if(rhs.get_type() == e_CELL::FredkinCell)
            p = new Fredkin(*((Fredkin*)(rhs.p)));
        else
            p = new Conway(*((Conway*)(rhs.p)));
    }

    ~Cell()
    {
        delete p;
    }

};

#endif //  Cell_h