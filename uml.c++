@startuml
Life   "1" *- "*" Cell
Cell "1" o- "1" AbstractClass
AbstractClass <|-- FredkinCell
AbstractClass <|-- ConwayCell

class Life {
    - vector<vector<T>> grid
    - int generation
    - int population
    - int count_live_neighbors(size_t r, size_t c)
    - bool is_cell_state(size_t r, size_t c)

    + Life(int r, int c)
    + void add_cell(size_t r, size_t c)
    + void tick()
    + void write_board(ostream& w)
}

class Cell {
    - AbstractCell *p;
    - bool get_next_state(int n_state)
    - string to_string() const
    - void set_alive()
    - void update_state() const
    - e_CELL get_type() const
    - bool is_alive() const

    + Cell()
    + Cell(const Cell& rhs)

}

class  AbstractClass{
    {abstract}
    # bool state;
    # bool next_state;
    # virtual bool get_next_state(int n_state) = 0;
    # virtual string to_string() const = 0;
    # virtual e_CELL get_type() const = 0;
    # void set_alive()
    # void update_state()
    # bool is_alive() const

    + AbstractCell()
}

class ConwayCell {
    - bool get_next_state(int n_state)
    - string to_string()
    - e_CELL get_type()
    - void set_alive()
    - void update_state()
    - bool is_alive() const

    + Conway()
}

class  FredkinCell {
    - int age
    - bool get_next_state(int n_state)
    - string to_string()
    - e_CELL get_type()
    - void set_alive()
    - void update_state()
    - bool is_alive() const

    + Fredkin()
}

@enduml
